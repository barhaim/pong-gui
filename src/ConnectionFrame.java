import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JSeparator;

import java.awt.Component;
import java.io.BufferedReader;
//import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;

import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener;

import javax.swing.Box;
import javax.swing.JComboBox;


public class ConnectionFrame extends JFrame implements SerialPortEventListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/* Serial Port */
	SerialPort serialPort;
	
	private BufferedReader input;
	//private OutputStream output;

	private ArrayList<CommPortIdentifier> Com_Ports;

	JComboBox<String> comboBox;
	JLabel lblWaitingToMsps;

	/**
	 * Create the frame.
	 */
	public ConnectionFrame() {
		Com_Ports = new ArrayList<CommPortIdentifier>();


		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 430);
		setTitle("Welcome to Pong..");
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(6, 1, 0, 0));

		JLabel label = new JLabel("Embedded Pong!");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.BOLD, 30));
		contentPane.add(label);

		JSeparator separator = new JSeparator();
		contentPane.add(separator);

		lblWaitingToMsps = new JLabel("Select Server COM Port: ");
		lblWaitingToMsps.setFont(new Font("Arial", Font.PLAIN, 16));
		lblWaitingToMsps.setVerticalAlignment(SwingConstants.TOP);
		lblWaitingToMsps.setHorizontalAlignment(SwingConstants.CENTER);
		lblWaitingToMsps.setForeground(new Color(255, 255, 255));
		contentPane.add(lblWaitingToMsps);

		comboBox = new JComboBox<String>();
		comboBox.setEditable(false);

		contentPane.add(comboBox);

		Component verticalStrut = Box.createVerticalStrut(20);
		contentPane.add(verticalStrut);

		JButton btnStartAGame = new JButton("Start a new match");
		btnStartAGame.setForeground(new Color(0, 0, 0));
		btnStartAGame.setBackground(new Color(135, 206, 250));
		btnStartAGame.setFont(new Font("Lucida Console", Font.BOLD, 19));
		contentPane.add(btnStartAGame);

		initializeSerial(); // Initialize Serial

		btnStartAGame.addActionListener(new CustomActionListener());


	}

	class CustomActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			
			/* Check if serial server is connected to port */
			CommPortIdentifier portId = null;	
			portId = Com_Ports.get(comboBox.getSelectedIndex());
			
			// Check if COM is connected
			if (portId == null){
				System.out.println("Could not find COM Port.");
				lblWaitingToMsps.setText("Could not find COM Port.");
				return;
			}

			dispose();
			close();
			PongWindow window = new PongWindow (portId.getName());
			window.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
			window.setVisible (true);
		}
	}

	public void initializeSerial(){
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();


		while (portEnum.hasMoreElements()){
			CommPortIdentifier currentPortIdentifier = (CommPortIdentifier) portEnum.nextElement();
			
			System.out.println(currentPortIdentifier.getName());
			Com_Ports.add(currentPortIdentifier); // Add port to list
			
			comboBox.addItem(currentPortIdentifier.getName());
		}

		
		/*
		// Connect to Serial port
		try {
			// Open serial port
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);

			// Set port parameters
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// Open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// Add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);

		} catch(Exception e){
			System.err.println(e.toString());
		}
		*/

	}

	@Override
	public void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE){
			try {
				int inputKey=input.read();
				System.out.println(inputKey);
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

}
