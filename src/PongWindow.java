import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.Timer;



public class PongWindow extends JFrame {

	private static final long serialVersionUID = -1257952525274008504L;
	private String Com_Port;

	public PongWindow (String Com_Port) {
		super();
		this.Com_Port = Com_Port;
		
		setTitle("Pong");
		setSize(800, 600);
		
		
		Pong content = new Pong (Player.MSP, Player.MSP, Com_Port);
		content.acceleration = false;
		getContentPane().add (content);
		
		Timer timer = new Timer (20, content);
		timer.start ();
	}
	
	
}
