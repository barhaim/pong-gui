import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;

import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;


public class Pong extends JPanel implements ActionListener, MouseListener, SerialPortEventListener {

	private static final int RADIUS = 10; 
	private static int START_SPEED = 5; 
	private static final int ACCELERATION = 50; 

	private static final int SPEED = 12; 
	private static final int HEIGHT = 50;
	private static final int WIDTH = 20;
	private static final int TOLERANCE = 5;
	private static final int PADDING = 10;

	private static final int UP_P1 = 'w';
	private static final int DOWN_P1 = 's';
	private static final int UP_P2 = 'o';
	private static final int DOWN_P2 = 'l';

	private Player player1;
	private Player player2;

	private boolean new_game = true;

	private int ball_x;
	private int ball_y;
	private double ball_x_speed;
	private double ball_y_speed;

	public boolean acceleration = false;
	private int ball_acceleration_count;

	private boolean mouse_inside = false;

	private boolean key_up_p1 = false;
	private boolean key_down_p1 = false;
	private boolean key_up_p2 = false;
	private boolean key_down_p2 = false;

	/* Serial Port */
	SerialPort serialPort;

	private String Com_Port;

	private BufferedReader input;
	private OutputStream output;

	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;

	private boolean isPaused = false;

	// Constructor
	public Pong (int p1_type, int p2_type, String COM_PORT) {
		super ();
		setBackground (new Color (0, 160, 252));

		player1 = new Player (p1_type);
		player2 = new Player (p2_type);

		/* Serial Initialize port */
		Com_Port = COM_PORT;
		initializeSerial();
		Play_Background_Music();
	}

	// Play music
	private void Play_Background_Music(){
		try {
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			InputStream is = new BufferedInputStream(new FileInputStream(new File("bg-music.mid")));
			sequencer.setSequence(is);
			sequencer.start();
			sequencer.setLoopCount(5);
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidMidiDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void Play_Finish_Music(){
		try {
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			InputStream is = new BufferedInputStream(new FileInputStream(new File("bg-music.mid")));
			sequencer.setSequence(is);
			sequencer.start();
			sequencer.setLoopCount(5);
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidMidiDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Compute destination of the ball
	private void computeDestination (Player player) {
		if (ball_x_speed > 0)
			player.destination = ball_y + (getWidth() - PADDING - WIDTH - RADIUS - ball_x) * (int)(ball_y_speed) / (int)(ball_x_speed);
		else
			player.destination = ball_y - (ball_x - PADDING - WIDTH - RADIUS) * (int)(ball_y_speed) / (int)(ball_x_speed);

		if (player.destination <= RADIUS)
			player.destination = 2 * PADDING - player.destination;

		if (player.destination > getHeight() - 10) {
			player.destination -= RADIUS;
			if ((player.destination / (getHeight() - 2 * RADIUS)) % 2 == 0)
				player.destination = player.destination % (getHeight () - 2 * RADIUS);
			else
				player.destination = getHeight() - 2 * RADIUS - player.destination % (getHeight () - 2 * RADIUS);
			player.destination += RADIUS;
		}
	}

	// Set new position of the player
	private void movePlayer (Player player, int destination) {
		int distance = Math.abs (player.position - destination);

		if (distance != 0) {
			int direction = - (player.position - destination) / distance;

			if (distance > SPEED)
				distance = SPEED;

			player.position += direction * distance;

			if (player.position - HEIGHT < 0)
				player.position = HEIGHT;
			if (player.position + HEIGHT > getHeight())
				player.position = getHeight() - HEIGHT;
		}
	}

	// Compute player position
	private void computePosition (Player player, int Player_Num) {

		// KEYBOARD
		if (player.getType() == Player.MSP) {
			if (key_up_p1 && !key_down_p1 && (Player_Num == 1)) { // P1 Up
				movePlayer (player, player.position - SPEED);
			}
			else if (key_down_p1 && !key_up_p1 && (Player_Num == 1)) { // P1 Down
				movePlayer (player, player.position + SPEED);
			}
			else if (key_up_p2 && !key_down_p2 && (Player_Num == 2)) { // P2 Up
				movePlayer (player, player.position - SPEED);
			}
			else if (key_down_p2 && !key_up_p2 && (Player_Num == 2)) { // P2 Down
				movePlayer (player, player.position + SPEED);
			}
		}

		// CPU HARD
		else if (player.getType() == Player.CPU_HARD) {
			movePlayer (player, player.destination);
		}
		// CPU EASY
		else if (player.getType() == Player.CPU_EASY) {
			movePlayer (player, ball_y);
		}
	}

	// Draw
	public void paintComponent (Graphics g) {
		super.paintComponent (g);

		if (new_game) {
			/* Initialize new game */
			ball_x = getWidth () / 2;
			ball_y = getHeight () / 2;

			double phase = Math.random () * Math.PI / 2 - Math.PI / 4;
			ball_x_speed = (int)(Math.cos (phase) * START_SPEED);
			ball_y_speed = (int)(Math.sin (phase) * START_SPEED);

			ball_acceleration_count = 0;

			/* Put both controllers in the middle of the screen */
			if (player1.getType() == Player.MSP || player1.getType() == Player.CPU_HARD || player1.getType() == Player.CPU_EASY) {
				player1.position = getHeight () / 2;
				computeDestination (player1);
			}
			if (player2.getType() == Player.MSP || player2.getType() == Player.CPU_HARD || player2.getType() == Player.CPU_EASY) {
				player2.position = getHeight () / 2;
				computeDestination (player2);
			}

			new_game = false;
		}

		if (player1.getType() == Player.MSP ){
			computePosition (player1, 1);
			key_down_p1 = false;
			key_up_p1 = false;
		}

		if (player2.getType() == Player.MSP){
			computePosition (player2, 2);
			key_down_p2 = false;
			key_up_p2 = false;
		}

		ball_x += ball_x_speed;
		ball_y += ball_y_speed;
		if (ball_y_speed < 0) // Hack to fix double-to-int conversion
			ball_y ++;

		if (acceleration) {
			ball_acceleration_count ++;
			if (ball_acceleration_count == ACCELERATION) {
				ball_x_speed = ball_x_speed + (int)ball_x_speed / Math.hypot ((int)ball_x_speed, (int)ball_y_speed) * 2;
				ball_y_speed = ball_y_speed + (int)ball_y_speed / Math.hypot ((int)ball_x_speed, (int)ball_y_speed) * 2;
				ball_acceleration_count = 0;
			}
		}

		// Border-collision LEFT
		if (ball_x <= PADDING + WIDTH + RADIUS) {
			int collision_point = ball_y + (int)(ball_y_speed / ball_x_speed * (PADDING + WIDTH + RADIUS - ball_x));
			if (collision_point > player1.position - HEIGHT - TOLERANCE && 
					collision_point < player1.position + HEIGHT + TOLERANCE) {
				ball_x = 2 * (PADDING + WIDTH + RADIUS) - ball_x;
				ball_x_speed = Math.abs (ball_x_speed);
				ball_y_speed -= Math.sin ((double)(player1.position - ball_y) / HEIGHT * Math.PI / 4)
						* Math.hypot (ball_x_speed, ball_y_speed);
				if (player2.getType() == Player.CPU_HARD)
					computeDestination (player2);
			}
			else {
				player2.points ++;
				
				try {
					output.write('g');
					output.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(player1.points > 9){
					System.out.println("Player 2 Won!");
					//default title and icon
					//JOptionPane.showMessageDialog(frame,
					//    "Eggs are not supposed to be green.");
					player1.points = 0;
					player2.points = 0;
				}
				new_game = true;
			}
		}

		// Border-collision RIGHT
		if (ball_x >= getWidth() - PADDING - WIDTH - RADIUS) {
			int collision_point = ball_y - (int)(ball_y_speed / ball_x_speed * (ball_x - getWidth() + PADDING + WIDTH + RADIUS));
			if (collision_point > player2.position - HEIGHT - TOLERANCE && 
					collision_point < player2.position + HEIGHT + TOLERANCE) {
				ball_x = 2 * (getWidth() - PADDING - WIDTH - RADIUS ) - ball_x;
				ball_x_speed = -1 * Math.abs (ball_x_speed);
				ball_y_speed -= Math.sin ((double)(player2.position - ball_y) / HEIGHT * Math.PI / 4)
						* Math.hypot (ball_x_speed, ball_y_speed);
				if (player1.getType() == Player.CPU_HARD)
					computeDestination (player1);
			}
			else {
				player1.points ++;
				
				try {
					output.write('r');
					output.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(player1.points > 9){
					System.out.println("Player 1 Won!");
					player1.points = 0;
					player2.points = 0;
				}
				new_game = true;
			}
		}

		// Border-collision TOP
		if (ball_y <= RADIUS) {
			ball_y_speed = Math.abs (ball_y_speed);
			ball_y = 2 * RADIUS - ball_y;
		}

		// Border-collision BOTTOM
		if (ball_y >= getHeight() - RADIUS) {
			ball_y_speed = -1 * Math.abs (ball_y_speed);
			ball_y = 2 * (getHeight() - RADIUS) - ball_y;
		}

		g.setColor (Color.WHITE);
		g.fillRect (PADDING, player1.position - HEIGHT, WIDTH, HEIGHT * 2);
		g.fillRect (getWidth() - PADDING - WIDTH, player2.position - HEIGHT, WIDTH, HEIGHT * 2);

		g.fillOval (ball_x - RADIUS, ball_y - RADIUS, RADIUS*2, RADIUS*2);

		g.setFont(new Font("Tahoma", Font.BOLD, 25));
		g.drawString (player1.points+" ", getWidth() / 2 - 20, 20);
		g.drawString (player2.points+" ", getWidth() / 2 + 20, 20);
		
	}

	// New frame
	public void actionPerformed (ActionEvent e) {
		if(!isPaused)
		repaint ();
	}

	// Mouse inside
	public void mouseEntered (MouseEvent e) {
		mouse_inside = true;
	}

	// Mouse outside
	public void mouseExited (MouseEvent e) {
		mouse_inside = false;
	}

	// Mouse pressed
	public void mousePressed (MouseEvent e) {}

	// Mouse released
	public void mouseReleased (MouseEvent e) {}

	// Mouse clicked
	public void mouseClicked (MouseEvent e) {}

	@Override
	public void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE){
			try {
				int inputKey=input.read();

				if (inputKey == UP_P1)
					key_up_p1 = true;
				else if (inputKey == DOWN_P1)
					key_down_p1 = true;
				else if (inputKey == UP_P2)
					key_up_p2 = true;
				else if (inputKey == DOWN_P2)
					key_down_p2 = true;
				else if (inputKey == '9'){
					//change ball speed
					ball_x_speed = ball_x_speed * 1.5;
					ball_y_speed = ball_y_speed * 1.5;

				}else if (inputKey == '0'){
					//change ball speed
					ball_x_speed = ball_x_speed / 1.5;
					ball_y_speed = ball_y_speed / 1.5;	
				}

				else if (inputKey == 'd'){
					/// pause the game
					isPaused = true;
				} 
				
				else if (inputKey == 'c'){
					/// pause the game
					isPaused = false;
					
				}

				System.out.println(inputKey);
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
	}

	public void initializeSerial(){
		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		while (portEnum.hasMoreElements()){
			CommPortIdentifier currentPortIdentifier = (CommPortIdentifier) portEnum.nextElement();
			if (currentPortIdentifier.getName().equals(Com_Port)){ 
				portId = currentPortIdentifier;
				break;
			}
		}

		// Check if COM is connected
		if (portId == null){
			System.out.println("Could not find COM Port.");
			return;
		}

		// Connect to Serial port
		try {
			// Open serial port
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);

			// Set port parameters
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// Open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// Add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);

		} catch(Exception e){
			System.err.println(e.toString());
		}

	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}
	

}
